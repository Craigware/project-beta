from common.json import ModelEncoder
from .models import Appointment, Technician


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id"
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "customer",
        "technician"
    ]
    encoders = { "technician": TechnicianEncoder() }

    def get_extra_data(self, o):
        return { 'vin': o.auto.vin }
