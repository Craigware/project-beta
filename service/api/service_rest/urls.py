from django.urls import path
from service_rest.views import *


urlpatterns = [
    path("appointments/", api_list_appointments, name="list_appointments"),
    path("appointments/<int:pk>/", api_detail_appointments, name="detail_appointment"),
    path("appointments/<int:pk>/cancel/", api_cancel_appointment, name="cancel_appointment"),
    path("appointments/<int:pk>/finish/", api_finish_appointment, name="finish_appointment"),
    path("technicians/", api_list_technicians, name="list_technicians"),
    path("technicians/<int:pk>/", api_detail_technicians, name="detail_technician"),
]
