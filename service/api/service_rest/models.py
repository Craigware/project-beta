from django.db import models


class Technician(models.Model):
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250, null=True)
    employee_id = models.PositiveIntegerField()


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=250)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField()
    status = models.CharField(max_length=50)
    customer = models.CharField(max_length=250)
    auto = models.ForeignKey(
        AutomobileVO,
        related_name="auto",
        on_delete=models.CASCADE,
        null=True
    )
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE
    )
