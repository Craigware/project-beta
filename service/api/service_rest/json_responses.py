from django.http import JsonResponse
from .encoders import TechnicianEncoder, AppointmentEncoder


def json_invalid():
    return JsonResponse(
        {"message": "Invalid json"},
        status=400
    )


def json_technician_id_not_found(pk):
    return JsonResponse(
        {"message": f"Employee of id number {pk} does not exist."},
        status=404
    )


def json_technician_deleted(pk):
    return JsonResponse(
        {"message": f"Employee id number {pk} has been deleted."},
        status=200
    )


def json_encoded_technician(technician):
    return JsonResponse(
        technician,
        encoder=TechnicianEncoder,
        safe=False,
        status=200
    )


def json_encoded_technician_list(technician_list):
    return JsonResponse(
        {"technicians": technician_list},
        encoder=TechnicianEncoder,
        status=200
    )


def json_encoded_appointment_list(appointments_list):
    return JsonResponse(
        {"appointments": appointments_list},
        encoder=AppointmentEncoder,
        status=200
    )


def json_encoded_appointment(appointment):
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
        status=200
    )


def json_appointment_deleted(pk):
    return JsonResponse(
        {"message": f"Appointment of id {pk} has been deleted."},
        status=200
    )


def json_appointment_not_found(pk):
    return JsonResponse(
        {"message": f"Appointment of id {pk} was not found."},
        status=404
    )


def json_auto_vin_not_found(vin):
    return JsonResponse(
        {"message": f"Automobile with the vin {vin} was not found."},
        status=404
    )
