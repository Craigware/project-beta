import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()


from sales_rest.models import AutoMobileVO


def get():
    response = requests.get('http://inventory-api:8000/api/automobiles/')
    content = json.loads(response.content)
    for autos in content['autos']:
        AutoMobileVO.objects.update_or_create(
            href = autos['href'],
            defaults={
            'vin': autos['vin'],
            }
        )


def poll():
    while True:
        print('Sales poller polling for data')
        try:
            get()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
