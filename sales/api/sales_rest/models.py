from django.db import models


class AutoMobileVO(models.Model):
    href = models.CharField(max_length=100, unique=True)
    vin = models.CharField(max_length=250, unique=True)


class SalesPerson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100)


class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=15)


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutoMobileVO,
        related_name='automobile',
        on_delete=models.CASCADE,
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name='salesperson',
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name='customer',
        on_delete=models.CASCADE,
    )
    price = models.IntegerField()
