from common.json import ModelEncoder
from .models import AutoMobileVO, SalesPerson, Customer, Sale


class AutoMobileEncoder(ModelEncoder):
    model = AutoMobileVO
    properties = [
        'id',
        'href',
        'vin',
    ]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        'id',
        'first_name',
        'last_name',
        'employee_id',
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        'id',
        'first_name',
        'last_name',
        'address',
        'phone_number',
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        'id',
        'automobile',
        'sales_person',
        'customer',
        'price',
    ]
    encoders = {
        'automobile': AutoMobileEncoder(),
        'sales_person': SalesPersonEncoder(),
        'customer': CustomerEncoder(),
    }
