from django.shortcuts import render
from .models import AutoMobileVO, SalesPerson, Customer, Sale
from django.views.decorators.http import require_http_methods
import json


from django.http import JsonResponse
from .encoders import AutoMobileEncoder, SalesPersonEncoder, CustomerEncoder, SaleEncoder


@require_http_methods(["GET", "POST"])
def sales_person_list(request):
    if request.method == "GET":
        sales_person = SalesPerson.objects.all()
        return JsonResponse(
            {'sales_person': sales_person},
            encoder=SalesPersonEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
            {"sales_person": sales_person},
            encoder=SalesPersonEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def sales_person_detail(request, pk):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                {"sales_person": sales_person},
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {'error': 'ID does not exist'},
                status = 400,
            )
    elif request.method == "DELETE":
        try:
            count, _ = SalesPerson.objects.filter(id=pk).delete()
            return JsonResponse(
                {"deleted": count > 0}
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {'error': 'ID does not exist'},
                status = 400,
            )
    else:
        try:
            content = json.loads(request.body)
            SalesPerson.objects.filter(id=pk).update(**content)
            sales_person = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                {"sales_person": sales_person},
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {'error': 'ID does not exist'},
                status = 400,
            )


@require_http_methods(["GET", "POST"])
def customer_list(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {'customer': customer},
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def customer_detail(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                {"customer": customer},
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {'error': 'ID does not exist'},
                status = 400,
            )
    elif request.method == "DELETE":
        try:
            count, _ = Customer.objects.filter(id=pk).delete()
            return JsonResponse(
                {"deleted": count > 0}
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {'error': 'ID does not exist'},
                status = 400,
            )
    else:
        try:
            content = json.loads(request.body)
            Customer.objects.filter(id=pk).update(**content)
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                {"customer": customer},
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {'error': 'ID does not exist'},
                status = 400,
            )


@require_http_methods(["GET", "POST"])
def sale_list(request):
    if request.method == "GET":
        sale = Sale.objects.all()
        return JsonResponse(
            {'sale': sale},
            encoder=SaleEncoder,
            safe=False,
        )

    else:
        content = json.loads(request.body)
        try:
            automobile_href = content['automobile']
            automobile = AutoMobileVO.objects.get(href=automobile_href)
            content['automobile'] = automobile
        except AutoMobileVO.DoesNotExist:
            return JsonResponse(
                {'error': 'Automobile does not exist'},
                status=400,
            )

        try:
            salesperson_id = content['sales_person']
            salesperson = SalesPerson.objects.get(id=salesperson_id)
            content['sales_person'] = salesperson
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {'error': 'Automobile does not exist'},
                status=400,
            )

        try:
            customer_id = content['customer']
            customer = Customer.objects.get(id=customer_id)
            content['customer'] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {'error': 'Automobile does not exist'},
                status=400,
            )
        print(content);
        sale = Sale.objects.create(**content)
        return JsonResponse(
            {'sale': sale},
            encoder=SaleEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def sale_detail(request, pk):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=pk)
            return JsonResponse(
                {'sale': sale},
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {'error': 'ID does not exist'},
                status = 400,
            )
    elif request.method == "DELETE":
        try:
            count, _ = Sale.objects.filter(id=pk).delete()
            return JsonResponse(
                {'deleted': count > 0}
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {'error': 'ID does not exist'},
                status = 400,
            )
    else:
        try:
            content = json.loads(request.body)
            try:
                automobile_id = content['automobile']
                automobile = AutoMobileVO.objects.get(id=automobile_id)
                content['automobile'] = automobile
            except AutoMobileVO.DoesNotExist:
                return JsonResponse(
                    {'error': 'ID does not exist'},
                    status = 400,
                )

            try:
                sales_person_id = content['sales_person']
                sales_person = SalesPerson.objects.get(id=sales_person_id)
                content['sales_person'] = sales_person
            except SalesPerson.DoesNotExist:
                return JsonResponse(
                    {'error': 'ID does not exist'},
                    status = 400,
                )

            try:
                customer_id = content['customer']
                customer = Customer.objects.get(id=customer)
                content['customer'] = customer
            except Customer.DoesNotExist:
                return JsonResponse(
                    {'error': 'ID does not exist'},
                    status = 400,
                )

            Sale.objects.filter(id=pk).update(**content)
            sale = Sale.objects.get(id=pk)
            return JsonResponse(
                {'sale': sale},
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {'error': 'ID does not exist'},
                status = 400,
            )


@require_http_methods(["GET"])
def automobile_sold(request):
    automobiles = AutoMobileVO.objects.all()
    automobile_not_sold = []
    for automobile in automobiles:
        try:
            Sale.objects.get(automobile=automobile.id)
        except Sale.DoesNotExist:
            automobile_not_sold.append(automobile)
    return JsonResponse(
        {'notsold': automobile_not_sold},
        encoder=AutoMobileEncoder,
        safe=False,
    )
