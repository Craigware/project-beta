from django.urls import path
from .views import sales_person_detail, sales_person_list, customer_detail, customer_list, sale_detail, sale_list, automobile_sold

urlpatterns = [
    path('/salespeople/', sales_person_list, name='sales_people'),
    path('/salespeople/<int:pk>/', sales_person_detail, name='sales_people_detail'),
    path('/customers/', customer_list, name='customer'),
    path('/customers/<int:pk>/', customer_detail, name='customer_detail'),
    path('/sales/', sale_list, name='sales'),
    path('/sales/<int:pk>/', sale_detail, name='sales_detail'),
    path('/autocheck/', automobile_sold, name='autocheck'),
]
