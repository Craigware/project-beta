# CarCar

Team:

* Craig - Service Microservice
* Cyrus - Sales Microservice

## Design

## Service microservice

I'm going to create 3 Models, one for the Technician, one for the AutoVO, and one for the Apointment. <br />
I'm going to pull a vin ID from the Automobile Inventory API and create a AutoVO with the vin. <br />
I'm going to create RESTFUL API urls through view functions. <br />
I'm going to use those APIs to post, and get information from the service APIs. <br />
Hopefully I will be able to use delete and put methods as well <br />
I'm going to create dynamic webpages with react around the content using API calls. <br />

## Sales microservice

I will create 4 models including ones for: salesperson, customers, sales and automobileVO <br />
The automobileVO will use a vin field from the Automobile inventory API<br />
Dynamic webpages will be made via RESTFUL API's in conjunction with django and react frameworks. <br />
