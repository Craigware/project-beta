import React, {useEffect, useState} from 'react';


function AppointmentForm(){
    const [formData, setFormData] = useState({
        date_time: "",
        reason: "",
        customer: "",
        employee_id: "",
        vin: ""
    });
    const [technicians, setTechnicians] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);
    const [customers, setCustomers] = useState([]);


    const fetchTechnicians = async () => {
        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const technicianResponse = await fetch(technicianUrl);
        if (technicianResponse.ok) {
            const technicianData = await technicianResponse.json();
            setTechnicians(technicianData.technicians);
        }
    }


    const fetchAutomobiles = async () => {
        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const automobileResponse = await fetch(automobileUrl);
        if (automobileResponse.ok) {
            const automobileData = await automobileResponse.json();
            setAutomobiles(automobileData.autos);
        }
    }


    const fetchCustomers = async () => {
        const customersUrl = 'http://localhost:8090/api/customers/';
        const customersResponse = await fetch(customersUrl);
        if (customersResponse.ok) {
            const customersData = await customersResponse.json();
            setCustomers(customersData.customer);
        }
    }


    const handleFormData = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
            }
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok){
            setFormData({
                date_time: "",
                reason: "",
                customer: "",
                employee_id: "",
                vin: ""
            });
        }
    }


    useEffect(() => {
        fetchAutomobiles();
        fetchTechnicians();
        fetchCustomers();
    }, [])


    return (
        <div className="row">
            <div className="offset-2 col-8">
                <div className="shadow-lg p-4 mt-4">
                    <h1>Create Appointment</h1>
                    <form className="mt-3" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <select className="form-control mb-3" onChange={handleFormData} value={formData.customer} required id="customer" name="customer">
                                <option value="">Select a customer</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={customer.phone_number} value={`${customer.first_name} ${customer.last_name}`}>
                                            {customer.first_name} {customer.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                            <label htmlFor="customer" className="text-muted">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <textarea className="form-control" onChange={handleFormData} value={formData.reason} type="text" required id="reason" name="reason" />
                            <label htmlFor="reason" className="text-muted" >Reason for Appointment</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input className="form-control" onChange={handleFormData} value={formData.date_time} type="datetime-local" required id="date_time" name="date_time"/>
                            <label htmlFor="date_time" className="text-muted">Appointment Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select className="form-control mb-3" value={formData.vin} onChange={handleFormData} required id="vin" name="vin">
                                <option value="">Select an automobile</option>
                                {automobiles.map(automobile => {
                                    return (
                                        <option key={automobile.vin} value={automobile.vin}>
                                            {automobile.model.name} {automobile.vin}
                                        </option>
                                    );
                                })}
                            </select>
                            <label htmlFor="vin" className="text-muted">Automobile VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select className="form-control" value={formData.employee_id} onChange={handleFormData} required id="employee_id" name="employee_id">
                                <option value="">Select a technician</option>
                                {technicians.map(technician =>{
                                    return (
                                        <option key={technician.employee_id} value={technician.employee_id}>
                                            {technician.first_name} {technician.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                            <label htmlFor="employee_id" className="text-muted">Technician</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <button className="btn btn-primary">Create Appointment</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}


export default AppointmentForm;
