import React, {useEffect, useState} from "react";


function ApointmentList(){
    const [appointments, setAppointments] = useState([]);
    const [sales, setSales] = useState([]);


    const fetchAppointments = async () => {
        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const appointmentResponse = await fetch(appointmentUrl);
        if (appointmentResponse.ok) {
            const appointmentData = await appointmentResponse.json();
            setAppointments(appointmentData.appointments);
        }
    }


    const fetchSales = async () => {
        const salesUrl = 'http://localhost:8090/api/sales/';
        const salesResponse = await fetch(salesUrl);
        if (salesResponse.ok) {
            const salesData = await salesResponse.json();
            setSales(salesData.sale);
        }
    }


    const handleStatus = async (event) => {
        event.preventDefault();
        const status = event.target.value;
        const id = event.target.id;
        const appointmentUrl = `http://localhost:8080/api/appointments/${id}/${status}`;
        const fetchConfig = {
            method: "put",
            headers: {
                "Content-Type": "application/json",
            }
        };

        await fetch(appointmentUrl, fetchConfig);
        fetchAppointments();
    }

    const deleteData = async (event) => {
        event.preventDefault();
        const id = event.target.value;
        const appointmentUrl = `http://localhost:8080/api/appointments/${id}/`;
        const fetchConfig = {
            method: "delete",
        };

        await fetch(appointmentUrl, fetchConfig);
        fetchAppointments();
    }


    useEffect(() => {
        fetchAppointments();
        fetchSales();
    }, []);


    return (
        <div>
            <h1 className="display-6 mt-2"><strong>Appointments</strong></h1>
            <table className="table table-striped mt-2 table-hover">
                <thead className="heading-success">
                    <tr>
                        <th>Customer</th>
                        <th>VIP Status</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Reason</th>
                        <th>Status</th>
                        <th>Technician</th>
                        <th>Mark Canceled</th>
                        <th>Mark Complete</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody className="">
                    {appointments.map(appointment => {
                        const dateTime = new Date(appointment.date_time);
                        const time = dateTime.toLocaleTimeString([], {hour: '2-digit', minute: "2-digit"});
                        const date = dateTime.toLocaleDateString();
                        const vipStatus = () => {
                            const salesContainsVip = sales.filter(sale => sale.automobile.vin === appointment.vin);
                            if (salesContainsVip.length > 0){
                                return "TRUE";
                            }
                            return "FALSE";
                        }

                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.customer}</td>
                                <td>{vipStatus()}</td>
                                <td>{date}</td>
                                <td>{time}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.status}</td>
                                <td>{appointment.technician.first_name}</td>
                                <td>
                                    <button id={appointment.id} onClick={handleStatus} value="cancel" className="btn btn-warning">Cancel</button>
                                </td>
                                <td>
                                    <button id={appointment.id} onClick={handleStatus} value="finish" className="btn btn-primary">Finished</button>
                                </td>
                                <td>
                                    <button onClick={deleteData} value={appointment.id} className="btn btn-danger">Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}


export default ApointmentList;
