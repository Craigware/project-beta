import { useState, useEffect } from "react";


function AutomobileList(){
    const [automobile, setAutomobile] = useState([]);


    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok){
            const data = await response.json();
            setAutomobile(data.autos);
        }
    }


    const deleteData = async (event) => {
        event.preventDefault();
        const id = event.target.value;
        const automobileUrl = `http://localhost:8080/api/automobiles/${id}/`;
        const fetchConfig = {
            method: "delete",
        };

        await fetch(automobileUrl, fetchConfig);
        getData();
    }


    useEffect(() => {
        getData();
    }, []);


    return(
        <table className="table">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Sold</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {automobile.map((autos) => {
                    return (
                        <tr key={autos.id}>
                            <td>{autos.vin}</td>
                            <td>{autos.year}</td>
                            <td>{autos.model.name}</td>
                            <td>{autos.model.manufacturer.name}</td>
                            <td>{autos.sold}</td>
                            <td>
                                <button onClick={deleteData} value={autos.id} className="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );

}


export default AutomobileList;
