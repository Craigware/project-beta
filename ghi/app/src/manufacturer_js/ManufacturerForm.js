import React, {useState} from "react";


function ManufacturerForm(){
    const [manufacturer, setManufacturer] = useState("");


    const handleManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {"name": manufacturer};

        const url = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': "application/json",
            }
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok){
            setManufacturer("");
        }
    }


    return (
        <div className="row">
            <div className="offset-2 col-8">
                <div className="shadow-lg p-4 mt-4">
                    <h1>Create Manufacturer</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3 mt-3">
                            <input onChange={handleManufacturer} value={manufacturer} className="form-control" required type="text" name="manufacturer" id="manufacturer" />
                            <label htmlFor="manufacturer" className="text-muted">Manufacturer Name</label>
                        </div>
                        <button className="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    )
}


export default ManufacturerForm;
