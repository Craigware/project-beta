import React, {useState, useEffect} from "react";


function SalesPersonForm(){
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
    });


    useEffect(() => {
    }, []);


    const handleSubmit = async(e) => {
        e.preventDefault();
        const url = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if(response.ok){
            setFormData({
                first_name: '',
                last_name: '',
                employee_id: '',
            });
        }
    }


    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    }


    return(
        <div className="row">
            <div className="offset-2 col-8">
                <div className="shadow-lg p-4 mt-4">
                <h1>Create Salesperson</h1>
                    <form onSubmit={handleSubmit} id="create_salesperson">
                        <div className="form-floating mb-3">
                            <input className="form-control"onChange={handleFormChange} value={formData.first_name} placeholder="First Name" required type="text" name="first_name" id="first_name" />
                            <label htmlFor="first_name" className="text-muted">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input className="form-control" onChange={handleFormChange} value={formData.last_name} placeholder="Last Name" required type="text" name="last_name" id="last_name" />
                            <label htmlFor="last_name" className="text-muted">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input className="form-control" onChange={handleFormChange} value={formData.employee_id} placeholder="Employee ID" required type="text" name="employee_id" id="employee_id"/>
                            <label htmlFor="employee_id" className="text-muted">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create Sales Person</button>
                    </form>
                </div>
            </div>
        </div>
    );
}


export default SalesPersonForm;
