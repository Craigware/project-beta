import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import TechnicianForm from './technician_js/TechnicianForm';
import TechnicianList from './technician_js/TechnicianList';
import AppointmentForm from './appointment_js/AppointmentForm';
import AppointmentList from './appointment_js/AppointmentList';
import AppointmentHistory from './appointment_js/AppointmentHistory';
import ManufacturerList from './manufacturer_js/ManufacturerList';
import ManufacturerForm from './manufacturer_js/ManufacturerForm';
import ModelList from './model_js/ModelList';
import ModelForm from './model_js/ModelForm';
import Nav from './Nav';
import AutomobileForm from './automobile_js/AutomobileForm';
import AutomobileList from './automobile_js/AutomobileList';
import CustomerForm from './customer_js/CustomerForm';
import CustomerList from './customer_js/CustomerList';
import SalesForm from './sales_js/SalesForm';
import SalesList from './sales_js/SalesList';
import SalesPersonForm from './salesperson_js/SalesPersonForm';
import SalesPersonList from './salesperson_js/SalesPersonList';
import SalesPersonSelect from './salesperson_js/SalesPersonSelect';


function App() {
  return (
    <BrowserRouter>
        <Nav />
        <Routes>
            <Route path="/" element={<MainPage />} />

            <Route path="/salespeople/">
                <Route path="" element={<SalesPersonList />} />
                <Route path="create" element={<SalesPersonForm />} />
                <Route path="select" element={<SalesPersonSelect />} />
            </Route>

            <Route path="/customers/">
                <Route path="" element={<CustomerList />} />
                <Route path="create" element={<CustomerForm />} />
            </Route>


            <Route path="/sales/">
                <Route path="" element={<SalesList />} />
                <Route path="create" element={<SalesForm />} />
            </Route>


            <Route path="/automobiles/">
                <Route path="" element={<AutomobileList />} />
                <Route path="create" element={<AutomobileForm />} />
            </Route>


            <Route path="/technicians/">
                <Route path="" element={<TechnicianList />} />
                <Route path="create" element={<TechnicianForm />} />
            </Route>
            <Route path="/appointments/">
                <Route path="" element={<AppointmentList />} />
                <Route path="create" element={<AppointmentForm />} />
                <Route path="history" element={<AppointmentHistory />} />
            </Route>
            <Route path="/manufacturers/">
                <Route path="" element={<ManufacturerList />} />
                <Route path="create" element={<ManufacturerForm />} />
            </Route>
            <Route path="/models/">
                <Route path="" element={<ModelList />} />
                <Route path="create" element={<ModelForm />} />
            </Route>
        </Routes>
    </BrowserRouter>
  );
}


export default App;
