import React, {useEffect, useState} from "react";


function ModelForm(){
    const [formData, setFormData] = useState({
        "name": "",
        "picture_url": "",
        "manufacturer_id": "",
    });
    const [manufacturers, setManufacturers] = useState([]);


    const handleFormData = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        })
    }


    const fetchManufacturers = async () => {
        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const manufacturerResponse = await fetch(manufacturerUrl);
        if (manufacturerResponse.ok) {
            const manufacturerData = await manufacturerResponse.json();
            setManufacturers(manufacturerData.manufacturers);
        }
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': "application/json",
            }
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok){
            setFormData({
                "name": "",
                "picture_url": "",
                "manufacturer_id": "",
            });
        }
    }


    useEffect(() => {
        fetchManufacturers();
    }, []);


    return (
        <div className="row">
            <div className="offset-2 col-8">
                <div className="shadow-lg p-4 mt-4">
                    <h1>Create Model</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3 mt-3">
                            <input onChange={handleFormData} value={formData.name} className="form-control" required type="text" name="name" id="name" />
                            <label htmlFor="name" className="text-muted">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormData} value={formData.picture_url} className="form-control" required type="url" name="picture_url" id="picture_url" />
                            <label htmlFor="picture_url" className="text-muted">Picture URL</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleFormData} value={formData.manufacturer_id} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                                <option value="">Select A Manufacturer</option>
                                {manufacturers.map(manufacturer => {
                                    return (
                                        <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                    )
                                })}
                            </select>
                            <label htmlFor="manufacturer" className="text-muted">Select A Manufacturer</label>
                        </div>
                        <button className="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    )
}


export default ModelForm;
