import React, {useEffect, useState} from "react";


function ModelList(){
    const [models, setModels] = useState([]);


    const fetchModels = async () => {
        const modelUrl = 'http://localhost:8100/api/models/';
        const modelResponse = await fetch(modelUrl);
        if (modelResponse.ok) {
            const modelData = await modelResponse.json();
            setModels(modelData.models);
        }
    }


    const deleteData = async (event) => {
        event.preventDefault();
        const id = event.target.value;
        const appointmentUrl = `http://localhost:8100/api/models/${id}/`;
        const fetchConfig = {
            method: "delete",
        };

        await fetch(appointmentUrl, fetchConfig);
        fetchModels();
    }


    useEffect(() => {
        fetchModels();
    }, []);


    return (
        <div className="mt-3">
            <h1 className="display-6 mt-2"><strong>Models</strong></h1>
            <table className="table table-striped mt-2 table-hover">
                <thead className="heading-success">
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody className="">
                    {models.map(model => {
                        return (
                            <tr key={model.name}>
                                <td>{model.name}</td>
                                <td>{model.manufacturer.name}</td>
                                <td><img src={model.picture_url} style={{maxHeight: "400px", maxWidth: "600px"}}/></td>
                                <td>
                                    <button onClick={deleteData} value={model.id} className="btn btn-danger">Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}


export default ModelList;
