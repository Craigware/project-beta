import React, {useState, useEffect} from "react";

function CustomerForm(){
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        address: '',
        phone_number: '',
    });

    useEffect(() => {
    }, []);

    const handleSubmit = async(e) => {
        e.preventDefault();
        const url = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if(response.ok){
            setFormData({
                first_name: '',
                last_name: '',
                address: '',
                phone_number: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    }

    return(
        <div className="row">
            <div className="offset-2 col-8">
                <div className="shadow-lg p-4 mt-4">
                    <h1>Create Customer</h1>
                    <form onSubmit={handleSubmit} id="create_salesperson">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.first_name} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                            <label htmlFor="first_name" className="text-muted">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.last_name} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                            <label htmlFor="last_name" className="text-muted">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.address} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                            <label htmlFor="address" className="text-muted">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.phone_number} placeholder="Phone Number" required type="text" name="phone_number" id="phone_number" className="form-control" />
                            <label htmlFor="phone_number" className="text-muted">Phone Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CustomerForm;
