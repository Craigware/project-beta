import React, { useEffect, useState } from 'react';


function TechnicianForm(){
    const [formData, setFormData] = useState({
        first_name: "",
        last_name: "",
        employee_id: ""
    });


    const handleFormData = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': "application/json",
            }
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok){
            setFormData({
                first_name: "",
                last_name: "",
                employee_id: ""
            });
        }
    }


    return (
        <div className="row">
            <div className="offset-2 col-8">
                <div className="shadow-lg p-4 mt-4">
                    <h1>Create Technician</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input className="form-control" onChange={handleFormData} value={formData.first_name} type="text" required id="first_name" name="first_name" />
                            <label htmlFor="first_name" className="text-muted">First Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input className="form-control" onChange={handleFormData} value={formData.last_name} type="text" required id="last_name" name="last_name" />
                            <label htmlFor="last_name" className="text-muted">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input className="form-control" onChange={handleFormData} value={formData.employee_id} type="text" required id="employee_id" name="employee_id" />
                            <label htmlFor="employee_id" className="text-muted">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create Technician</button>
                    </form>
                </div>
            </div>
        </div>
    );
}


export default TechnicianForm;
