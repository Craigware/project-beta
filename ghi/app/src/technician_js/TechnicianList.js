import React, {useEffect, useState} from "react";


function TechnicianList(){
    const [technicians, setTechnicians] = useState([]);


    const fetchTechnicians = async () => {
        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const technicianResponse = await fetch(technicianUrl);
        if (technicianResponse.ok) {
            const technicianData = await technicianResponse.json();
            setTechnicians(technicianData.technicians);
        }
    }


    const deleteData = async (event) => {
        event.preventDefault();
        const id = event.target.value;
        const technicianUrl = `http://localhost:8080/api/technicians/${id}/`;
        const fetchConfig = {
            method: "delete",
        };

        await fetch(technicianUrl, fetchConfig);
        fetchTechnicians();
    }


    useEffect(() => {
        fetchTechnicians();
    }, []);


    return (
        <div>
            <h1 className="display-6 mt-2"><strong>Technicians</strong></h1>
            <table className="table table-striped mt-2 table-hover">
                <thead className="heading-success">
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee ID</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody className="">
                    {technicians.map(technician => {
                        return (
                            <tr key={technician.employee_id}>
                                <td>{technician.first_name}</td>
                                <td>{technician.last_name}</td>
                                <td>{technician.employee_id}</td>
                                <td>
                                    <button onClick={deleteData} value={technician.employee_id} className="btn btn-danger">Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}


export default TechnicianList;
